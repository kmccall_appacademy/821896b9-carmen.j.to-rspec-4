class Book
  # TODO: your code goes here!

    def title
        @title
    end

    def title=(name)
        lowercase = ['the', 'a', 'an', 'and', 'in', 'of']
        new_title = []
        name.split.each_with_index do |word, i|
            if i == 0
                new_title << word.capitalize
            elsif lowercase.include?(word)
                new_title << word
            else
                new_title << word.capitalize
            end
        end
        @title = new_title.join(' ')
    end
end
