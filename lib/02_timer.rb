class Timer
    attr_accessor :seconds

    def initialize
        @seconds = 0
    end

    def time_string
        seconds = @seconds % 60
        @minutes = @seconds / 60
        minutes = @minutes % 60
        @hours = @minutes / 60
        hours = @hours % 60
        update_time([hours, minutes, seconds])
    end

    def update_time(arr)
        result = []
            arr.each do |n|
                if n < 10
                    result << "0" + n.to_s
                else
                    result << n
                end
            end
        result.join(":")
    end
end
